
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class RataRataArray {
    public static void main(String[] args) {
        int jml = 0, nilai[] = {90, 80, 70, 85, 95};
        double rata = 0;
        for (int i = 0; i < nilai.length; i++) 
            jml += nilai[i];
        rata = jml / nilai.length;
        System.out.println("Rata-rata nilai: "+rata);
        System.out.print("Grade : ");
        if (rata > 80) System.out.println("A");
        else if (rata > 70) System.out.println("B");
        else if (rata > 60) System.out.println("C");
        else if (rata > 40) System.out.println("D");
        else System.out.println("E");
    }
}
