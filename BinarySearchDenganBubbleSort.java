
import java.util.Scanner;

/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class BinarySearchDenganBubbleSort {
    public static void main(String[] args) {
        int[] nilai = {7,8,2,9,2,1,3,2,2};
        int cari = 0, indeks = -1;
        bubbleSort(nilai);
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan bilangan yang dicari : ");
        try {
            cari = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!"+e.getMessage());
        }
        indeks = binarySearch(cari, nilai);
        if (indeks < 0) System.out.println(cari+" tidak ditemukan pada daftar");
        else System.out.println(cari+" ditemukan pada indeks ke-"+indeks);
    }
    
    private static int binarySearch(int n, int[] data) {
        int awal = 0;                       // indeks awal array
        int akhir = data.length - 1;        // indeks akhir array
        int tengah = 0;                     // indeks tengah array
        
        while (awal <= akhir) {             
            tengah = (awal + akhir) / 2;    // set indeks tengah
            if (data[tengah] < n)           // nilai yang dicari ada pada paruh kanan array
                awal = tengah + 1;          // set ulang indeks awal
            else if (data[tengah] > n)      // nilai yang dicari ada pada paruh kiri array
                akhir = tengah - 1;         // set ulang indeks akhir
            else                            // nilai yang dicari ada pada data[tengah]
                return tengah;
        }
        return -1;                          // loop berakhir, tidak ada return tengah
    }
    
    /***
     * Melakukan bubble sort (pass by reference) terhadap array of integer
     * @param data array of integer dengan data yang belum disorting
     */
    private static void bubbleSort(int[] data) {
        int tmp;
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (data[i] > data[j]) {
                    tmp = data[i];
                    data[i]=data[j];
                    data[j]=tmp;
                }
            }
        }
    }
}
