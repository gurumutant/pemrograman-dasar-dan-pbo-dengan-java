
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class LinearSearch_V1 {
    public static void main(String[] args) {
        int[] nilai = {7,8,9,2,1,3};
        int cari = 0;
        boolean ketemu = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan bilangan yang dicari : ");
        try {
            cari = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!"+e.getMessage());
        }
        int i = 0;
        while (i < nilai.length) {
            if (nilai[i] == cari) {
                ketemu = true;
                break;
            }
            i++;
        }
        if (ketemu) {
            System.out.println("Bilangan "+cari+" ditemukan pada indeks ke-"+i);
        } else {
            System.out.println("Bilangan "+cari+" tidak ditemukan");
        }
    }
}
