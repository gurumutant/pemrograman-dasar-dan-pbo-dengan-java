
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class PokokeDeret {
    public static void main(String[] args) {
        int counter=0, n=0, a=0, b=1, tmp;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan sebuah bilangan asli >= 3: ");
        System.out.println("Deret :");
        try {
            n = sc.nextInt();   
        } catch (Exception e) {
            System.out.println("Kesalahan input !!");
            System.exit(1);
        }          
        while (counter < n) {
            tmp = b;
            b = b + a;            
            a = tmp;
            System.out.print(b+"\t");
            counter++;
        }     
    } 
}
