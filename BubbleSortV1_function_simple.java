/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class BubbleSortV1_function_simple {
    public static void main(String[] args) {
        int[] nilai = {7,8,9,2,1,3};     
        System.out.println("Nilai sebelum diurutkan : ");
        tampilNilai(nilai);
        System.out.println();
        bubbleSort(nilai);
        System.out.print("\nNilai setelah diurutkan : ");
        tampilNilai(nilai);
    }
    
    /***
     * Menampilkan nilai di array of integer secara horizontal 
     * dengan delimiter tab
     * @param Nilai array of integer dengan data nilai 
     */
    private static void tampilNilai(int[] Nilai) {
        System.out.println("");
        for (int i = 0; i < Nilai.length; i++) {
            System.out.print(Nilai[i]+"\t");
        }
    }
    
    /***
     * Melakukan bubble sort (pass by reference) terhadap array of integer
     * @param data array of integer dengan data yang belum disorting
     */
    private static void bubbleSort(int[] data) {
        int tmp;
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (data[i] > data[j]) {
                    tmp = data[i];
                    data[i]=data[j];
                    data[j]=tmp;
                }
            }
        }
    }
}
