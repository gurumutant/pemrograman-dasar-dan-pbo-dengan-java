
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Prima_V3 {
    public static void main(String[] args) {
        int r, bil, counter=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan sebuah bilangan asli: ");
        try {
            bil = sc.nextInt();
            for (r = 1; r<=bil;r++) {
                if (bil % r == 0) counter++;
            }        
            if (counter == 2) System.out.println(bil+" adalah bilangan prima");
            else System.out.println(bil+" bukanlah bilangan prima");    
        } catch (Exception e) {
            System.out.println("Kesalahan input !!");
        }          
    }    
}
