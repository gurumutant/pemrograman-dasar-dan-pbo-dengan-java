/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Person.Siswa;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public interface Statistical {

    public double hitungRerata(Siswa data[]);
    
    public void sorting(Siswa data1[]);
    
    public void searching(Siswa data2[]);

}
