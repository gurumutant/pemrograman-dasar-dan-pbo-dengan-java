/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Utama {
    static Guru g1 = new Guru();
    static Guru g2 = new Guru();
    static Guru g3 = new Guru();
    static BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
    static Siswa[] dataSiswa = new Siswa[5];
    static boolean GuruOK = false, SiswaOK = false;
    
    public static void main(String[] args) {
        int pil=0;
        System.out.println("*****PROGRAM APLIKASI DATA NILAI*****");
        System.out.println("1. Masukkan Data Guru");
        System.out.println("2. Masukkan Data Nilai Siswa");
        System.out.println("3. Tampilkan Data Guru");
        System.out.println("4. Tampilkan Data Nilai Siswa");
        System.out.println("5. Pengurutan Data Siswa");
        System.out.println("6. Pencarian Data Siswa");
        System.out.println("7. Keluar Program");
        System.out.println("*************************************");
        for (;;) {
            System.out.print("MASUKKAN KODE PILIHAN (1-7): ");
            try {
                pil = Integer.valueOf(rd.readLine());
            } catch (Exception e) {
                System.out.println("Kesalahan input !");
            }             
            switch (pil) {
                case 1: insertDataGuru(); break;
                case 2: insertDataSiswa(); break;
                case 3: tampilDataGuru(); break;
                case 4: tampilDataSiswa(); break;
                case 5: urutDataSiswa(); break;
                case 6: cariDataSiswa(); break;
                case 7: System.exit(0);
                default: System.err.println("Pilihan tidak tersedia !");
            }
        }
    }
    
    public static void insertDataGuru() {
        g1.setDataGuru("Guru-1", 101, "TI11-1", "Pemrograman-Dasar");
        g2.setDataGuru("Guru-2", 102, "TI11-2", "Bahasa-Indonesia");
        g3.setDataGuru("Guru-3", 103, "TI10-1", "Matematika Dasar");
        System.out.println("DATA BERHASIL DIMASUKKAN");
        System.out.println("=================================================="); 
        GuruOK = true;
    }

    public static void insertDataSiswa() {
        Siswa s;
        for (int i = 0; i < dataSiswa.length; i++) {
            s = new Siswa();
            System.out.println("Masukkan Data siswa ke-"+(i+1)+": ");
            s.setDataSiswa();
            dataSiswa[i] = s;
        }
        SiswaOK = true;
    }

    public static void tampilDataGuru() {
        if (GuruOK) {
            System.out.println("***** DATA GURU MATA PELAJARAN *****");
            System.out.println("------------------------------------");
            System.out.format("%-10s", "NIP");
            System.out.format("%-20s", "Nama");
            System.out.format("%-20s", "Mata Pelajaran");
            System.out.format("%-10s", "Kelas");
            System.out.println("");
            System.out.println("------------------------------------");
            g1.infoPerson();
            g2.infoPerson();
            g3.infoPerson();
            System.out.println("------------------------------------");
        } else {
            System.out.println("Data Guru belum dimasukkan!");
        }
    }

    public static void tampilDataSiswa() {
        if (SiswaOK) {
            System.out.println("***** DATA NILAI SISWA *****");
            System.out.println("Nama Kelas: "+g1.getKelas());        
            System.out.println("Nama Mapel: "+g1.getNamaMapel());        
            System.out.println("Bobot Nil UTS: 0.4 Bobot Nilai UAS: 0.6");
            System.out.println("----------------------------------------------------------");
            System.out.format("%-10s","NIS");
            System.out.format("%-20s","NAMA");
            System.out.format("%-10s","N.UTS");
            System.out.format("%-10s","N.UAS");
            System.out.format("%-10s","N.AKHIR");
            System.out.format("%-15s","N.HURUF");
            System.out.println("");
            System.out.println("----------------------------------------------------------");
            for (int i = 0; i < dataSiswa.length; i++) {
                    dataSiswa[i].infoPerson();
            }        
            System.out.println("----------------------------------------------------------");
            System.out.print("NILAI RATA-RATA SISWA: ");
            System.out.format("%3.2f",dataSiswa[0].hitungRerata(dataSiswa));
            System.out.println("");
        } else {
            System.out.println("Data siswa belum diinputkan !");
        }
    }

    public static void urutDataSiswa() {
        if (SiswaOK) { 
            dataSiswa[0].sorting(dataSiswa);
        } else {
            System.out.println("Data siswa belum diinputkan !");
        }
    }

    public static void cariDataSiswa() {
        if (SiswaOK) { 
            dataSiswa[0].searching(dataSiswa);
        } else {
            System.out.println("Data siswa belum diinputkan !");
        }
    }

}
