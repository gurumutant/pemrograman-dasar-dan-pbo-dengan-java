/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Guru extends Person {
    private int NIP;
    private String Kelas;
    private String namaMapel;

    public void setDataGuru(String nama, int NIP, String kelas, String namaMapel) {
        this.nama = nama;
        this.NIP = NIP;
        this.Kelas = kelas;
        this.namaMapel = namaMapel;
    }
    
    @Override
    public void infoPerson() {
        System.out.format("%-10s", this.NIP);
        System.out.format("%-20s", this.nama);
        System.out.format("%-20s", this.namaMapel);
        System.out.format("%-10s", this.Kelas);
        System.out.println("");
    }  
    
    public String getKelas() {
        return Kelas;
    }

    public String getNamaMapel() {
        return namaMapel;
    }
    
    
}
