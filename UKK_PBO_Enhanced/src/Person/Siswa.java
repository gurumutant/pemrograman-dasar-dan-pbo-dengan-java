/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Person;

import Interface.Statistical;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Siswa extends Person implements Statistical {
    private int NIS;
    private int nilUTS;
    private int nilUAS;
    private double nilAkhir;
    private String nilHuruf;
    private final double bbtUTS;
    private final double bbtUAS;
    BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

    public Siswa() {
        bbtUTS = 0.4;
        bbtUAS = 0.6;
    }
    
    public void setDataSiswa(int NIS, String nama, int nilUTS, int nilUAS) {
        this.NIS = NIS;
        this.nama = nama;
        this.nilUTS = nilUTS;
        this.nilUAS = nilUAS;
        hitungNilAkhir();
        nilHuruf = hitungNilHuruf();
    }
    
    public void setDataSiswa() {
        boolean valid = true; 
        int nis=0, niluts=0, niluas=0;
        String Nama="";
        do {
            System.out.print("Masukkan NIS : ");
            try {
                nis = Integer.valueOf(rd.readLine());
                valid = true;
            } catch (Exception ex) {
                System.err.println("Kesalahan input NIS. Harap ulangi lagi !");
                valid = false;
            }
        } while (!valid);
        do {
            System.out.print("Masukkan Nama : ");
            try {
                Nama = rd.readLine();
                if (Nama.length() > 0) valid = true;
                else {
                    valid = false;
                    System.err.println("Nama tidak boleh kosong !");
                }
            } catch (Exception ex) {
                System.err.println("Kesalahan input Nama. Harap ulangi lagi !");
            }
        } while (!valid);
        do {
            System.out.print("Masukkan NILAI UTS : ");
            try {
                niluts = Integer.valueOf(rd.readLine());
                valid = true;
            } catch (Exception ex) {
                System.err.println("Kesalahan input Nilai UTS. Harap ulangi lagi !");
                valid = false;
            }
        } while (!valid);
        do {
            System.out.print("Masukkan NILAI UAS : ");
            try {
                niluas = Integer.valueOf(rd.readLine());
                valid = true;
            } catch (Exception ex) {
                System.err.println("Kesalahan input NIS. Harap ulangi lagi !");
                valid = false;
            }
        } while (!valid);
        
        setDataSiswa(nis, Nama, niluts, niluas);
    }

    public void hitungNilAkhir() {
        this.nilAkhir = this.bbtUTS * this.nilUTS + this.bbtUAS * this.nilUAS;
    }

    public double getNilAkhir() {
        return nilAkhir;
    }
    
    public String hitungNilHuruf() {
        if (nilAkhir>90) return "A (SANGAT BAIK)";
        else if (nilAkhir>80) return "B (BAIK)";
        else if (nilAkhir>=70) return "C (CUKUP)";
        else return "K (KURANG)";
    }

    @Override
    public void infoPerson() {
        System.out.format("%-10s",NIS);
        System.out.format("%-20s",nama);
        System.out.format("%-10s",nilUTS);
        System.out.format("%-10s",nilUAS);
        System.out.format("%-10.2f",getNilAkhir());
        System.out.format("%-15s",hitungNilHuruf());
        System.out.println("");
    }
    
    @Override
    public double hitungRerata(Siswa[] data) {
        double rerata=0;
        for (int i = 0; i < data.length; i++) {
            rerata+=data[i].getNilAkhir();
        }
        rerata = rerata / data.length;
        return rerata;
    }

    @Override
    public void sorting(Siswa[] data1) {
        // menampilkan sebelum sorting
        System.out.println("DATA NILAI AKHIR SISWA SEBELUM DIURUTKAN");        
        System.out.println("----------------------------------------");
        System.out.println("NILAI AKHIR");
        System.out.println("----------------------------------------");
        for (int i = 0; i < data1.length; i++) {
            System.out.println(data1[i].getNilAkhir());
        }
        
        // proses sorting
        for (int i = 0; i < data1.length-1; i++) {
            for (int j = i+1; j < data1.length; j++) {
                if(data1[i].getNilAkhir() > data1[j].getNilAkhir()) {
                    Siswa temp = data1[i];
                    data1[i] = data1[j];
                    data1[j] = temp;
                }                 
            }
        }
        
        // menampilkan setelah sorting
        System.out.println("DATA NILAI AKHIR SISWA SETELAH DIURUTKAN");        
        System.out.println("----------------------------------------");
        System.out.println("NILAI AKHIR");
        System.out.println("----------------------------------------");
        for (int i = 0; i < data1.length; i++) {
            System.out.println(data1[i].getNilAkhir());
        }
    }

    @Override
    public void searching(Siswa[] data2) {
        System.out.println("DATA SISWA DENGAN PREDIKAT AMAT BAIK DAN CUKUP");
        System.out.println("----------------------------------------------------------");
        System.out.format("%-10s","NIS");
        System.out.format("%-20s","NAMA");
        System.out.format("%-10s","N.UTS");
        System.out.format("%-10s","N.UAS");
        System.out.format("%-10s","N.AKHIR");
        System.out.format("%-15s","N.HURUF");
        System.out.println("");
        for (int i = 0; i < data2.length; i++) {
            if(data2[i].hitungNilHuruf().equals("A (SANGAT BAIK)") ||
                    data2[i].hitungNilHuruf().equals("C (CUKUP)")) {
                data2[i].infoPerson();
            }
        }
    }
    
}
