/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class DeretGenap {
    public static void main(String[] args) {
        int m, n, r, s;
        n = 100;
        m = 0;
        r = 1; // deret bilangan asli
        s = 0; // variabel penampung hasil akhir penjumlahan deret
        while (m < n) {
            if (r % 2 == 0) {
                System.out.println(r);
                m++;
                s+=r;
            }
            r++;
        }
        System.out.println("-------------- +");
        System.out.println(s);
    }
}
