
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class LinearSearch_V1_string_while {

    public static void main(String[] args) {
        String[] nama = {"Ana","Ani","Tina","Toni","Tono","Joni","Jono"};
        String cari = "";
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan nama yang dicari : ");
        try {
            cari = sc.nextLine();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!"+e.getMessage());
        }
        int i = linearSearch(cari, nama);
        if (i>=0) {
            System.out.println("Nama "+cari+" ditemukan pada indeks ke-"+i);
        } else {
            System.out.println("Nama "+cari+" tidak ditemukan");
        }
    }
    
    /***
     * Fungsi untuk mencari jarum di tumpukan jerami menggunakan linear search
     * @param jarum string akan yang dicari pada array data
     * @param jerami string array yang memuat data 
     * @return index nilai yang dicari, bernilai -1 jika tidak ditemukan
     */
    private static int linearSearch(String jarum, String[] jerami) {
        int i = 0;
        while (i<jerami.length && !jerami[i].equalsIgnoreCase(jarum)) {
            i++;
        }
        return (i == jerami.length) ? -1 : i;
    }
}
