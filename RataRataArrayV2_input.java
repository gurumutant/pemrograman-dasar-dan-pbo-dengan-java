import java.util.Scanner;
import java.util.stream.IntStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class RataRataArrayV2_input {
    public static void main(String[] args) {     
        int jml = 0, n=0; 
        double rata=0;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan banyaknya nilai : ");
        try {
            n = in.nextInt();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!\n"+e.getMessage());
            System.exit(1);
        }
        int[] nilai = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Nilai ke-"+(i+1)+" = ");
            nilai[i] = in.nextInt();
        }
        jml = IntStream.of(nilai).sum();
        rata = jml / n;
        System.out.println("Rata-rata nilai: "+rata);
        
        if (rata > 80) System.out.println("A");
        else if (rata > 70) System.out.println("B");
        else if (rata > 60) System.out.println("C");
        else if (rata > 40) System.out.println("D");
        else System.out.println("E");
    }
}
