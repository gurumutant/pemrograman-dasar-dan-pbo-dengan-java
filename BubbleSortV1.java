
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class BubbleSortV1 {
    public static void main(String[] args) throws InterruptedException {
        int n=0, tmp=0; 
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan banyaknya nilai : ");
        try {
            n = in.nextInt();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!\n"+e.getMessage());
            System.exit(1);
        }
        int[] nilai = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Nilai ke-"+(i+1)+" = ");
            nilai[i] = in.nextInt();
        }
        System.out.println("Nilai sebelum diurutkan : ");
        for (int i = 0; i < nilai.length; i++) {
            System.out.print(nilai[i]+"\t");
        }
        System.out.println();
        // begin bubble sort
        int iterasi = 0; 
        for (int i = 0; i < n-1; i++) {
            for (int j=i+1;j<n;j++) {
                if (nilai[i] > nilai[j]) {
                    tmp = nilai[i];
                    nilai[i] = nilai[j];
                    nilai[j] = tmp; 
                    System.out.println(nilai[i]+" ditukar dengan "+nilai[j]);
                } else {
                    System.out.println("tidak ada penukaran. "+nilai[i]+"<="+nilai[j]);
                }
                iterasi++;
                // System.out.print("\nKondisi array nilai (i="+i+",j="+j+",iterasi ke-"+iterasi+"): ");
                //tampilNilai(nilai);
                Thread.sleep(1000);
            }
        }
        // end bubble sort
        System.out.print("\nNilai setelah diurutkan : ");
        tampilNilai(nilai);
        System.out.println("\nBanyaknya iterasi : "+iterasi);
    }
    
    private static void tampilNilai(int[] Nilai) {
        System.out.println("");
        for (int i = 0; i < Nilai.length; i++) {
            System.out.print(Nilai[i]+"\t");
        }
    }
}
