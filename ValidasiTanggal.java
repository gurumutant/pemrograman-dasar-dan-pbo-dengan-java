/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class ValidasiTanggal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ketikkan tanggal: ");
        String tgl = sc.next();
        sc.close();
        if (isDateValid(tgl)) 
            System.out.println("Tanggal "+tgl+" valid !");
        else
            System.out.println("Tanggal "+tgl+" tidak valid !");
    }
    
    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
