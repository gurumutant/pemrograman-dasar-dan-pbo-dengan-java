import java.util.Scanner;
/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class StudiKasusOlahDataDenganMenu {

    private static void inputData(int[] data) {
        int n;
        System.out.println("----- INPUT DATA ------\n");
        System.out.println("Masukkan nilai : ");
        Scanner scInput = new Scanner(System.in);
        for (int i = 0; i < data.length; i++) {
            System.out.print("Data ke-"+(i+1)+": ");
            try {
                data[i] = scInput.nextInt();
            } catch (Exception e) {
            }
        }
    }

    private static void tampilData(int[] data) {
        System.out.println("----- TAMPIL DATA ------\n");
        System.out.println("Isi data (bilangan bulat): ");
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i]+"\t");
        }
        System.out.println("");
    }

    private static void sortData(int[] data) {
        System.out.println("----- SORTING DATA ------\n");
        // silakan pilih mau menggunakan bubbleSort() atau selectionSort()
        // bubbleSort(data);
        selectionSort(data);
        System.out.println("Data telah diurutkan. Silakan cek dengan menu Tampil Data");
    }

    private static void searchData(int[] data) {
        int cari=0, indeks;
        System.out.println("----- SEARCH DATA ------\n");
        System.out.print("Ketikkan data yang dicari: ");
        Scanner scCari = new Scanner(System.in);
        try {
            cari = scCari.nextInt();
        } catch (Exception e) {
            System.out.println("Kesalahan input !");
        }
        // silakan pilih antara menggunakan linear search atau binary search
        // catatan, binary search akan otomatis mensorting data terlebih dahulu
        // indeks = linearSearch(cari, data);
        indeks = binarySearch(cari, data);
        if (indeks < 0) {
            System.out.println("Data tidak ditemukan");
        } else {
            System.out.println("Data ditemukan pada indeks ke-"+indeks);
        }
    }
    
    private static void bubbleSort(int[] data) {
        int tmp;
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = i+1; j < data.length; j++) {
                if (data[i] > data[j]) {
                    tmp = data[i];
                    data[i]=data[j];
                    data[j]=tmp;
                }
            }
        }
    }
    
    private static void selectionSort(int[] data) {
        int n = data.length;
        for (int i = 0; i < data.length - 1; i++) {
            int min_index = i;
            for (int j = i+1; j < data.length; j++) 
                if (data[j] < data[min_index]) min_index = j;
            if (min_index != i) {
                int tmp = data[min_index];
                data[min_index] = data[i];
                data[i] = tmp;
            }
        }
    }
    
    /***
     * Fungsi untuk mencari jarum di tumpukan jerami menggunakan linear search
     * @param jarum nilai int akan yang dicari pada array data
     * @param jerami int array yang memuat data 
     * @return index nilai yang dicari, bernilai -1 jika tidak ditemukan
     */
    private static int linearSearch(int jarum, int[] jerami) {
        int i = 0;
        while (i<jerami.length && jerami[i] != jarum) {
            i++;
        }
        return (i == jerami.length) ? -1 : i;
    }
    
    private static int binarySearch(int n, int[] data) {
        bubbleSort(data); // mengurutkan data sebelum searching...
       
        int awal = 0;                       // indeks awal array
        int akhir = data.length - 1;        // indeks akhir array
        int tengah = 0;                     // indeks tengah array
        
        while (awal <= akhir) {             
            tengah = (awal + akhir) / 2;    // set indeks tengah
            if (data[tengah] < n)           // nilai yang dicari ada pada paruh kanan array
                awal = tengah + 1;          // set ulang indeks awal
            else if (data[tengah] > n)      // nilai yang dicari ada pada paruh kiri array
                akhir = tengah - 1;         // set ulang indeks akhir
            else                            // nilai yang dicari ada pada data[tengah]
                return tengah;
        }
        return -1;                          // loop berakhir, tidak ada return tengah
    }
    
    
    public static void main(String[] args) {
        int jmlData=0;
        System.out.println("Berapa banyaknya data yang akan diolah : ");
        Scanner sc0 = new Scanner(System.in);
        try {
            jmlData=sc0.nextInt();
            
        } catch (Exception e) {
        }
        int[] nilai = new int[jmlData];
        int menu=0;
        while (menu != 5) {
            menu = pilihMenu();
            switch (menu) {
                case 1: inputData(nilai); break;
                case 2: tampilData(nilai); break;
                case 3: sortData(nilai); break;
                case 4: searchData(nilai); break;
                case 5: System.exit(0); break;
                default: System.out.println("Pilihan tidak tersedia !");
            }
        }
    }
    
    private static int pilihMenu() {
        System.out.println("============");
        System.out.println(" MENU UTAMA ");
        System.out.println("============");
        System.out.println("");
        System.out.println("1. Input Data");
        System.out.println("2. Tampil Data");
        System.out.println("3. Sorting Data");
        System.out.println("4. Searching Data");
        System.out.println("5. Keluar");
        System.out.println("");
        System.out.println("Pilih menu [1-5]: " );
        Scanner sc = new Scanner(System.in);
        try {
            return sc.nextInt();
        } catch (Exception e) {
            return 0;
        }
    }
}
