
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Prima_V2_deret {
    public static void main(String[] args) {
        Boolean p = true;
        int N, r, n=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan batas bilangan : ");
        try {
            N = sc.nextInt();
            System.out.println("Deret Bilangan prima hingga "+N+": ");
            for(n=2; n<=N;n++) {
                p = true;
                for (r = 2; r<=n/2;r++) {
                    if (n % r == 0) {
                        p = false;
                        break;
                    }
                }
                if (p) System.out.println(n);
            }        
        } catch (Exception e) {
            System.out.println("Kesalahan input !!");
        }          
    }    
}
