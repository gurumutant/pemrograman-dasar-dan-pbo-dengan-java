/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */
package PelatihanOOP;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Narasumber extends Pengguna {
    private String NIP;

    /**
     *
     * @return
     */
    public String getNIP() {
        return NIP;
    }

    /**
     *
     * @param NIP
     */
    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    @Override
    public void setPengguna() {
        Narasumber ns=new Narasumber();
        ns.setNIP("4432472347823");
        System.out.println("NIP Narasumber : ");
    }
    
    
}
