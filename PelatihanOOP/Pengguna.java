/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */
package PelatihanOOP;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
abstract public class Pengguna {
    private String Nama;
    private int jk;
    private int registrasi;

    /**
     *
     */
    public Pengguna () {
        this.jk = 0;
        this.registrasi = 0;
    }
    
    /**
     *
     * @return
     */
    public String getNama() {
        return Nama;
    }

    /**
     *
     * @param Nama
     */
    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    /**
     *
     * @return
     */
    public int getJk() {
        return jk;
    }

    /**
     *
     * @param jk
     */
    public void setJk(int jk) {
        this.jk = jk;
    }

    /**
     *
     * @return
     */
    public int getRegistrasi() {
        return registrasi;
    }

    /**
     *
     * @param registrasi
     */
    public void setRegistrasi(int registrasi) {
        this.registrasi = registrasi;
    }
    
    /**
     *
     */
    abstract public void setPengguna();
}
