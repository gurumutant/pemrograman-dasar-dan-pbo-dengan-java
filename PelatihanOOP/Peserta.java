/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */
package PelatihanOOP;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Peserta extends Pengguna {

    /**
     *
     */
    public Peserta[] data = new Peserta[2];
    private int id;
    private String Sekolah;
    private float Pretest;
    private float Posttest;

    /**
     *
     */
    public float NA;

    /**
     *
     */
    public char range;

    /**
     *
     */
    public String Ket;
    
    /**
     *
     */
    @Override
    public void setPengguna() {
        BufferedReader rdr = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Array Size: "+data.length);
        for (int i = 0; i < data.length; i++) {
            boolean valid = true;
            do {
            try {
                data[i]=new Peserta();
                System.out.println("Entry data peserta ke-"+(i+1));
                System.out.print("ID: ");
                data[i].setId(Integer.parseInt(rdr.readLine()));
                System.out.print("Nama: ");
                data[i].setNama(rdr.readLine());
                System.out.print("Jenis Kelamin (0=L,1=P): ");
                data[i].setJk(Integer.parseInt(rdr.readLine()));
                System.out.print("Registrasi (0=tidak hadir,1=hadir): ");
                data[i].setRegistrasi(Integer.parseInt(rdr.readLine()));
                System.out.print("Sekolah: ");
                data[i].setSekolah(rdr.readLine());
                System.out.print("Pre Test: ");
                data[i].setPretest(Float.parseFloat(rdr.readLine()));
                System.out.print("Post Test: ");
                data[i].setPosttest(Float.parseFloat(rdr.readLine()));
                valid = true;
            } catch (Exception ex) {
                System.out.println("Kesalahan input ! Silakan ulangi lagi.");
                valid = false;
            }
            } while (!valid);
        }
        System.out.println("");
    }
    
    /**
     *
     */
    public void infoPengguna() {
        System.out.format("%-10s","ID");
        System.out.format("%-10s","Nama");
        System.out.format("%-10s","JK");
        System.out.format("%-10s","Reg");
        System.out.format("%-10s","Sekolah");
        System.out.format("%-10s","Pre");
        System.out.format("%-10s","Post");
        System.out.format("%-10s","NA");
        System.out.format("%-10s","Range");
        System.out.format("%-10s","Ket");
        System.out.println("");
        for (int i = 0; i < data.length; i++) {
            System.out.format("%-10s", data[i].getId());
            System.out.format("%-10s", data[i].getNama());
            System.out.format("%-10s", (data[i].getJk() == 0) ? "L" : "P");
            System.out.format("%-10s", (data[i].getRegistrasi()== 0) ? "Tdk. Hadir" : "Hadir");
            System.out.format("%-10s", data[i].getSekolah());
            System.out.format("%-10s", data[i].getPretest());
            System.out.format("%-10s", data[i].getPosttest());
            System.out.format("%-10.2f", data[i].NA());
            System.out.format("%-10s", data[i].range());
            System.out.format("%-10s", data[i].Ket());
            System.out.println("");
        }
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getSekolah() {
        return Sekolah;
    }

    /**
     *
     * @param Sekolah
     */
    public void setSekolah(String Sekolah) {
        this.Sekolah = Sekolah;
    }

    /**
     *
     * @return
     */
    public float getPretest() {
        return Pretest;
    }

    /**
     *
     * @param Pretest
     */
    public void setPretest(float Pretest) {
        this.Pretest = Pretest;
    }

    /**
     *
     * @return
     */
    public float getPosttest() {
        return Posttest;
    }

    /**
     *
     * @param Posttest
     */
    public void setPosttest(float Posttest) {
        this.Posttest = Posttest;
    }

    /**
     *
     * @return
     */
    public float NA() {
        NA = (0.4f * Pretest) + (0.6f * Posttest);
        return NA;
    }
    
    /**
     *
     * @return
     */
    public char range() {
        if (this.NA > 80) range = 'A';
        else if (this.NA > 70) range = 'B';
        else if (this.NA > 60) range = 'C';
        else if (this.NA > 50) range = 'D';
        else range = 'E';
        return range;
    }
    
    /**
     *
     * @return
     */
    public String Ket() {
        if (this.range == 'A' || this.range == 'B' || this.range == 'C') {
            this.Ket = "Lulus";
            return this.Ket;
        } else {
            this.Ket = "Tidak Lulus";
            return this.Ket;
        }
    }
}
