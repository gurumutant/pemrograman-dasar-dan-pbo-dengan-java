/*
 * Tanpa lisensi yang spesifik. 
 * Anda bebas untuk menggunakan atau memodifikasinya.
 */
package PelatihanOOP;

import java.util.Scanner;

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class MainClass {
    static Peserta pst = new Peserta();

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        int menu;
        do {
            menu = pilihMenu();
            switch (menu) {
                case 1: inputData(); break;
                case 2: tampilData(); break;
                case 3: sortData(); break;
                case 4: searchData(); break;
                case 5: System.exit(0); break;
                default: System.out.println("Pilihan tidak tersedia !");
            }
        } while (menu != 5);

    }

    private static int pilihMenu() {
        System.out.println("============");
        System.out.println(" MENU UTAMA ");
        System.out.println("============");
        System.out.println("");
        System.out.println("1. Input Data");
        System.out.println("2. Tampil Data");
        System.out.println("3. Sorting Data");
        System.out.println("4. Searching Data");
        System.out.println("5. Keluar");
        System.out.println("");
        System.out.println("Pilih menu [1-5]: " );
        Scanner sc = new Scanner(System.in);
        try {
            return sc.nextInt();
        } catch (Exception e) {
            return 0;
        }
    }

    private static void inputData() {
        pst.setPengguna();
    }

    private static void tampilData() {
        pst.infoPengguna();
    }

    private static void sortData() {
        Peserta tmp=new Peserta();
        for (int i = 0; i < pst.data.length - 1; i++) {
            for (int j = 0+1; j < pst.data.length; j++) {
                if (pst.data[i].NA() > pst.data[j].NA()) {
                    tmp.data[j] = pst.data[i];
                    pst.data[i] = pst.data[j];
                    pst.data[j] = tmp.data[j];
                }
            }
        }
        System.out.println("Data setelah mengalami pengurutan : ");
        pst.infoPengguna();
    }

    private static void searchData() {
        int idCari=0;
        Scanner sc = new Scanner(System.in);
        System.out.print("\nKetikkan ID peserta: ");
        boolean valid = true;
        do {
            try {
                idCari = sc.nextInt();
                valid = true;
            } catch (Exception e) {
                System.out.println("Kesalahan input ! Gunakan bilangan bulat untuk ID.");
                valid = false;
            }
        } while (!valid);
        // linear search
        int indeks = -1;
        for (int i = 0; i < pst.data.length; i++) {
            if (pst.data[i].getId() == idCari) {
                indeks++;
                break;
            }           
        }
        if (indeks < 0) {
            System.out.println("\nData tidak ditemukan !");
        } else {
            System.out.println("\nData ditemukan !");
        }
    }

}
