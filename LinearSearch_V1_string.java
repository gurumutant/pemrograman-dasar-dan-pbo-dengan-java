
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class LinearSearch_V1_string {
    public static void main(String[] args) {
        String[] nama = {"Ana","Ani","Tina","Toni","Tono","Joni","Jono"};
        String cari = "";
        boolean ketemu = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan nama yang dicari : ");
        try {
            cari = sc.nextLine();
        } catch (Exception e) {
            System.out.println("Kesalahan input !!"+e.getMessage());
        }
        int i = 0;
        while (i < nama.length) {
            if (nama[i].equalsIgnoreCase(cari)) {
                ketemu = true;
                break;
            }
            i++;
        }
        if (ketemu) {
            System.out.println("Nama "+cari+" ditemukan pada indeks ke-"+i);
        } else {
            System.out.println("Nama "+cari+" tidak ditemukan");
        }
    }
}
