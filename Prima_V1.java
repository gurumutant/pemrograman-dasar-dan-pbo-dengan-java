
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class Prima_V1 {
    public static void main(String[] args) {
        boolean p = true;
        int r, n=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan sebuah bilangan asli: ");
        try {
            n = sc.nextInt();
            for (r = 2; r<n;r++) {
                if (n % r == 0) p = false;
            }        
            if (p && n > 1) System.out.println(n+" adalah bilangan prima");
            else System.out.println(n+" bukanlah bilangan prima");    
        } catch (Exception e) {
            System.out.println("Kesalahan input !!");
        }          
    }    
}
