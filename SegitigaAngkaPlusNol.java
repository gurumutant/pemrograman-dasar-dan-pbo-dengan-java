import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hendri Winarto [pacitan@gmail.com]
 */
public class SegitigaAngkaPlusNol {
    public static void main(String[] args) {
        int n=0;
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Masukkan dimensi segitiga : ");
            n = sc.nextInt();
        } catch (Exception e) {
            System.out.println("kesalahan input !!");
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                // System.out.print(((j <= i) ? "1": "0")+"\t");
                if (j<=i) {
                    System.out.print("1\t");
                } else {
                    System.out.print("0\t");
                }
            }
            System.out.println();
        }      
    }
}